<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="?&page=home" class="brand-link">
      <span class="brand-text font-weight-light" style="padding-left: 20%">Administration</span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="?&admin=index" class="nav-link <?php if($_GET['admin'] === 'index'){ echo "active"; } ?>">
                        <i class="nav-icon fa fa-tachometer"></i>
                        <p>
                            Tableau de bord
                        </p>
                    </a>
                </li>
                
                <li class="nav-item has-treeview <?php if($_GET['admin'] === 'caroussel' || $_GET['admin'] === 'site'){ echo "menu-open"; } ?>">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fa fa-cog"></i>
                        <p>
                            Configuration
                            <i class="fa fa-angle-left right"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="?&admin=site" class="nav-link <?php if($_GET['admin'] === 'site'){ echo "active"; } ?>">
                            <i class="fa fa-sitemap nav-icon"></i>
                            <p>Site</p>
                            </a>
                        </li>
                    </ul>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="?&admin=caroussel" class="nav-link <?php if($_GET['admin'] === 'caroussel'){ echo "active"; } ?>">
                            <i class="fa fa-camera nav-icon"></i>
                            <p>Photo Accueil</p>
                            </a>
                        </li>
                    </ul>    
                </li> 

                <?php if($User->hasPermission($_SESSION['id'], "ACCESS_RANK")){ ?>
                <li class="nav-item">
                    <a href="?&admin=grade" class="nav-link <?php if($_GET['admin'] === 'grade'){ echo "active"; } ?>">
                        <i class="nav-icon fa fa-graduation-cap"></i>
                        <p>
                            Grade
                        </p>
                    </a>
                </li>
                <?php } ?>
                <?php if($User->hasPermission($_SESSION['id'], "ACCESS_USERS")){ ?>
                <li class="nav-item">
                    <a href="?&admin=user" class="nav-link <?php if($_GET['admin'] === 'user' || $_GET['admin'] === 'modifyuser'){ echo "active"; } ?>">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            Joueurs
                        </p>
                    </a>
                </li>
                <?php } ?>
                <li class="nav-item">
                    <a href="?&admin=actuality" class="nav-link <?php if($_GET['admin'] === 'actuality'){ echo "active"; } ?>">
                        <i class="nav-icon fa fa-file-text"></i>
                        <p>
                            Actualité
                        </p>
                    </a>
                </li>
            </ul>
        </nav>    
    </div>
</aside>