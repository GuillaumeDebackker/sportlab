<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADMIN | <?php echo $title; ?></title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="admin/css/adminlte.min.css">
    <link rel="stylesheet" href="admin/css/style.css">
    <script src="https://cdn.tiny.cloud/1/6utclfc11k1bzt3ze76q9stc7ayzmigdlni30fen7l24ji7o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
      tinymce.init({
        selector: '#tinymce',
        menubar: '',
        height: 500,
        plugins: [
            'advlist autolink link image lists charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
            'table emoticons template paste help'
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media | forecolor backcolor emoticons'
      });
    </script>

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php include 'header.php' ?>
        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <h1 class="m-0 text-dark"><?php echo $title ?></h1>
                </div>
            </div>

            <?php echo $contentAdmin; ?>
        </div>
        <?php include 'footer.php' ?>
    </div>

        <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="admin/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="admin/js/adminlte.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="admin/js/demo.js"></script>
</body>
</html>