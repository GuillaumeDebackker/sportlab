<?php
    $title = "Actualité";

    if(isset($_GET['mode'])){
        if($_GET['mode'] === "addactuality"){
            if(isset($_FILES['image'])){
                $fileName = $_FILES['image']['name'];
                $fileSize =$_FILES['image']['size'];
                $fileTmp =$_FILES['image']['tmp_name'];
                $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

                $extensions= array("jpeg","jpg","png");
        
                if(in_array($fileType,$extensions)=== false){
                    $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                }

                if($fileSize > 2097152){
                    $errors[]='File size must be excately 2 MB';
                }
                if(empty($errors)){
                    move_uploaded_file($fileTmp,'upload/'.$fileName);

                    $Actuality->addActuality($_POST['title'],$_POST['text'],$fileName,$_SESSION['id']);
                    header("location: index.php?&admin=actuality");
                }
            }
        }
        if($_GET['mode'] === "delete"){
            $Actuality->removeActuality($_GET['id']);
        }
        if($_GET['mode'] === "editactuality"){
            $Actuality->updateActuality($_GET['id'],"title",$_POST['title']);
            $Actuality->updateActuality($_GET['id'],"textcontent",$_POST['text']);
        }
    }
?>

<div class="content">
    <div class="container-fluid">

        <?php include 'admin/page/actuality/actualityList.php'; ?>

        <?php if(isset($_GET['mode']) && ($_GET['mode'] === "add" || $_GET['mode'] === "edit")){ ?>
            <form method="POST" action="?&admin=actuality&mode=<?php echo ($_GET['mode'] === "add" ? "addactuality" : "editactuality&id=".$_GET['id']); ?>" class="card" enctype="multipart/form-data">
                <div class="card-header">
                    <h3 class="card-title"><?php echo ($_GET['mode'] === "add" ? "Ajouter" : "Editer"); ?> une actualité</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group">
                                <label for="title">Titre</label>
                                <input type="text" name="title" id="title" class="form-control" value="<?php if($_GET['mode'] === "edit"){echo $Actuality->getActuality($_GET['id'])['title'];} ?>">
                            </div>
                        </div>
                        <?php if($_GET['mode'] === "add"){ ?>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="exampleInputFile">Mettre un fichier</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="mb-3">
                        <textarea name="text" id="tinymce"><?php if($_GET['mode'] === "edit"){echo $Actuality->getActuality($_GET['id'])['textcontent'];} ?></textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <div style="padding-left: 95%;">
                        <button type="submit" class="btn btn-primary right"><?php echo ($_GET['mode'] === "add" ? "Ajouter" : "Editer"); ?></button>
                    </div>
                </div>
            </form>
            <div style="padding-top:30px;"></div>
        <?php } ?>
    </div>
</div>