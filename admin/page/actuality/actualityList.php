<div class="card">
    <div class="card-header">
        <h3 class="card-title">Liste des actualités</h3>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width:10px;">ID</th>
                    <th style="width:20%;">Titre</th>
                    <th style="width:20%;">Auteur</th>
                    <th style="width:10%;">Nom du fichier</th>
                    <th style="width:20%;">Crée le</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php if(count($Actuality->getAllActuality()) === 0){
                echo "<tr><td></td><td>Vous n'avez pas d'actualité</td><td></td><td></td><td></td><td></td></tr>";
            }else{
                foreach($Actuality->getAllActuality() as $actuality){
                    include 'admin/page/actuality/actualityViewList.php';
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <div style="padding-left:95%"> 
            <a href="?&admin=actuality&mode=add" class="btn btn-primary">Rajouter</a>
        </div>
    </div>
</div>