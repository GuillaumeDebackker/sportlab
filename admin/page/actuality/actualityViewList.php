<tr>
    <th><?php echo $actuality['id']; ?></th>
    <th><?php echo $actuality['title']; ?></th>
    <th><?php echo $User->getUser($actuality['author'])['lastname'] . " " . $User->getUser($actuality['author'])['firstname']; ?></th>
    <th><?php echo $actuality['filename']; ?></th>
    <th><?php echo $actuality['createDate']; ?></th>
    <th>
        <a class="btn btn-info btn-sm" href="?&admin=actuality&mode=edit&id=<?php echo $actuality['id'] ?>">
            <i class="fa fa-pencil">
            </i>
            Editer
        </a>
        <a class="btn btn-danger btn-sm" href="?&admin=actuality&mode=delete&id=<?php echo $actuality['id'] ?>">
            <i class="fa fa-trash">
            </i>
            Supprimer
        </a>
    </th>
</tr>