<?php
    require 'controller/view/Caroussel.php';

    $title = "Photo Accueil";
    $Caroussel = new Caroussel($databaseConnection);

    if(isset($_GET['mode']) && $_GET['mode'] === 'add'){
        if(isset($_FILES['image'])){
            $fileName = $_FILES['image']['name'];
            $fileSize =$_FILES['image']['size'];
            $fileTmp =$_FILES['image']['tmp_name'];
            $fileType = pathinfo($fileName, PATHINFO_EXTENSION);

            $extensions= array("jpeg","jpg","png");
      
            if(in_array($fileType,$extensions)=== false){
                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }

            if($fileSize > 2097152){
                $errors[]='File size must be excately 2 MB';
            }
            if(empty($errors)){
                move_uploaded_file($fileTmp,'upload/'.$fileName);

                $Caroussel->addCaroussel($_POST['title'],$_POST['description'],$fileName);
                header("location: index.php?&admin=caroussel");
            }
        }
    }else if(isset($_GET['mode']) && $_GET['mode'] === 'delete'){
        $Caroussel->removeCaroussel($_GET['id']);
        header("location: index.php?&admin=caroussel");
    }
?>

<div class="content">
    <div class="container-fluid">
        
        <div class="callout callout-info">
            <h5>Information</h5>
            <p>Les photos d'accueil permet de mettre en avant votre site. Une photo doit faire moins de 2MB et doit être en .jpeg/.jpg/.png</p>
        </div>

        <?php 
            include 'admin/page/caroussel/carousselCreate.php';
            include 'admin/page/caroussel/carousselList.php';
        ?>
        

    </div>
</div>