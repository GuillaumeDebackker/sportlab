<div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Rajouter une photo d'accueil</h3>
            </div>
            <form action="?&admin=caroussel&mode=add" method="POST" role="form" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="title">Titre</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Mettre un titre" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="textarea" class="form-control" name="description" id="description" placeholder="Mettre une description" required>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="exampleInputFile">Mettre un fichier</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile">Choisir un fichier</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                   <div style="padding-left:95%"> 
                       <button type="submit" class="btn btn-primary right">Rajouter</button>
                    </div>
                </div>
            </form>
</div>