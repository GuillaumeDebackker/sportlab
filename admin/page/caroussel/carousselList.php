<div class="card">
            <div class="card-header">
                <h3 class="card-title">Les photos d'accueil</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10px;">ID</th>
                            <th>titre</th>
                            <th>description</th>
                            <th>Nom du fichier</th>
                            <th style="width:40px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($Caroussel->getAllCaroussel() as $caroussel){
                                include 'admin/page/caroussel/carousselViewList.php';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
</div>