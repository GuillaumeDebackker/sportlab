<?php
    require 'controller/data/permissions.php';
    $title = "Grade";

    if(isset($_GET['mode'])){
        if($_GET['mode'] === "addRank" || $_GET['mode'] === "editRank"){
            $titleRank = $_POST['title'];
            $descriptionRank = $_POST['description'];
            $permissionRank = StringUtils::convertArrayToString(StringUtils::removeElementToArray($_POST, array("title","description")), true);
            $color = $_POST['color'];

            if(strpos($permissionRank, "all") !== false){
                $permissionRank = StringUtils::convertArrayToString(StringUtils::removeElementToArray($permissions, array("*")), true);
            }

            if($_GET['mode'] === "editRank"){
                $Rank->updateRank($_GET['id'],$titleRank,$descriptionRank,$permissionRank,$color);
            }else{
                $Rank->addRank($titleRank,$descriptionRank,$permissionRank,$color);
            }
            header('location: index.php?&admin=grade');
        }
        if($_GET['mode'] === "delete"){
            $Rank->removeRank($_GET['id']);
            header('location: index.php?&admin=grade');
        }
    }
?>

<div class="content">
    <div class="container-fluid">

        <div class="callout callout-info">
            <h5>Information</h5>
            <p>Le grade sportlab est le grade le plus important, c'est un grade de secours en cas de panne. Seul les membres ont accès à ce grade</p>
        </div>

        <?php include 'admin/page/rank/rankList.php'; ?>

        <?php if(isset($_GET['mode']) && ($_GET['mode'] === "add" || $_GET['mode'] === "edit")){ ?>
            <div style="padding-top: 2em;">
                <div class="card">
                    <div class="card-header bg-lightblue color-palette">
                        <h3 class="card-title"><?php echo ($_GET['mode'] === "add" ? "Creation de Grade" : "Editer un grade") ?></h3>
                        <div class="card-tools">
                            <a href="?&admin=grade" class="btn btn-tool"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                    <form action="?&admin=grade<?php echo ($_GET['mode'] === "add" ? "&mode=addRank" : "&mode=editRank&id=". $_GET['id']) ?>" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="title">Titre</label>
                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo ($_GET['mode'] === "edit" ? $Rank->getRank($_GET['id'])["name"] : "") ?>" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control" name="description" id="description" value="<?php echo ($_GET['mode'] === "edit" ? $Rank->getRank($_GET['id'])['description'] : "") ?>" required></input>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label for="color">Couleur</label>
                                        <select class="form-control" name="color" id="color">
                                            <option value="gris">gris</option>
                                            <?php 
                                                $array = Array('rouge','bleu','vert','noir','jaune','orange');
                                                foreach($array as $color){
                                                    echo '<option value="'.$color.'">'.$color.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <h3 class="text-center">Permissions</h3>
                            <div class="form-group pt-5">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-check">
                                            <div class="icheck-primary d-inline ml-2">
                                                <input type="checkbox" onclick="onClickCheckBox()" class="option-input checkbox" name="all" id="all"/>
                                                <label for="all">Toute les permissions</label>
                                            </div>
                                            <span class="text" style="font-size:1.25em;"> : Avoir toutes les permissions</span>
                                        </div>
                                    </div>
                                    <?php foreach($permissions as $permission){
                                        if(strpos($permission["permission"],"*") === false){    
                                    ?>
                                        <div class="col-6">
                                            <div class="form-check">
                                                <div class="icheck-primary d-inline ml-2">
                                                    <input type="checkbox" onclick="onClickCheckBox()" class="option-input checkbox" name="<?php echo $permission['permission'] ?>" id="<?php echo $permission['permission'] ?>" <?php echo ($_GET['mode'] === "edit" && StringUtils::hasString($Rank->getRank($_GET['id'])['permission'], $permission['permission']) ? "checked" : "") ?>/>
                                                    <label for="<?php echo $permission['permission'] ?>"><?php echo $permission['name'] ?></label>
                                                </div>
                                                <span class="text" style="font-size:1.25em;"> : <?php echo $permission['description'] ?></span>
                                            </div>
                                        </div>
                                    <?php }} ?>  
                                </div>  
                            </div>
                        </div>
                        <div class="card-footer">
                            <div style="padding-left: 95%;">
                                <button type="submit" class="btn btn-primary right"><?php echo ($_GET['mode'] === "add" ? "Rajouter" : "Editer") ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <script>
                function onClickCheckBox(){
                    var checked = document.getElementById("all").checked
                    var checkboxs = document.querySelectorAll('input[type=checkbox]');
                    for (var i=0; i<checkboxs.length; i++) {
                        var checkbox = checkboxs[i];
                        if(checkbox.name !== "all"){
                            if(checked) checkbox.checked = false;
                            checkbox.disabled = checked;
                        }
                    }
                }
            </script>
        <?php } ?>
    </div>
</div>    