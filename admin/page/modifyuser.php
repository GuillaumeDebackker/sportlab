<?php
    $title = "Editer un utilisateur";

    if(!$User->hasPermission($_SESSION['id'], "MODIFY_USERS")){
        header("location: index.php?&admin=user");
    }

    if(isset($_GET['save'])){
        foreach(Array("firstname","lastname","email","rank") as $key){
            if(!empty($_POST[$key])){
                $User->updateUser($_GET['id'],$key, $_POST[$key]);
            }
        }
        header("location: index.php?&admin=user");
        return;
    }

    $user = $User->getUser($_GET['id']);
?>

<div class="content">
    <div class="container-fluid">
        <form action="?&admin=modifyuser&save=true&id=<?php echo $_GET['id'] ?>" method="post">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Modifier un utilisateur</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-3">
                            <div class="form-group">
                                <label for="firstname">Prénom</label>
                                <input type="text" class="form-control" name="firstname" id="firstname" value="<?php echo $user['firstname'] ?>" required>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="lastname">Nom de famille</label>
                                <input type="text" class="form-control" name="lastname" id="lastname" value="<?php echo $user['lastname'] ?>" required>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" value="<?php echo $user['email'] ?>" required>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label for="rank">Grade</label>
                                <select name="rank" id="rank" class="custom-select">
                                    <?php foreach($Rank->getAllRanks() as $rank){ ?>
                                        <option value="<?php echo $rank['id'] ?>" <?php if($rank['id'] === $user['rank']){ echo "selected";} ?>><?php echo $rank['name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div style="padding-left:95%"> 
                        <button type="submit" class="btn btn-primary right">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>