<td class="project-actions text-right">
    <a class="btn btn-info btn-sm" href="?&admin=grade&mode=edit&id=<?php echo $rank['id'] ?>">
        <i class="fa fa-pencil">
        </i>
        Editer
    </a>
    <a class="btn btn-danger btn-sm" href="?&admin=grade&mode=delete&id=<?php echo $rank['id'] ?>">
        <i class="fa fa-trash">
        </i>
        Supprimer
    </a>
</td>