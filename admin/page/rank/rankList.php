<div class="card">
            <div class="card-header">
                <h3 class="card-title">Grade</h3>
            </div>
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 20%">
                                Nom
                            </th>
                            <th style="width: 30%">
                                Description
                            </th>
                            <th style="width: 20%">
                                Permission
                            </th>
                            <th>
                                Couleur
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            foreach($Rank->getAllRanks() as $rank){
                                include 'admin/page/rank/rankViewList.php';
                            } 
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <?php if($User->hasPermission($_SESSION['id'], "MODIFY_RANK")){ ?>
                <div style="padding-left: 95%;">
                    <a href="?&admin=grade&mode=add" class="btn btn-primary right">Rajouter</a>
                </div>
                <?php } ?>
            </div>
</div>