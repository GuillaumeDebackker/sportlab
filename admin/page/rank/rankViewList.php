<tr>
    <td><?php echo $rank['id'] ?></td>
    <td><?php echo $rank['name'] ?></td>
    <td><?php echo $rank['description'] ?></td>
    <td><?php echo $rank['permission'] ?></td>
    <td><?php echo $rank['color'] ?></td>
    <?php
        if($User->hasPermission($_SESSION['id'], "MODIFY_RANK")){
            if(strpos($rank["id"], "2") === false || $User->hasPermission($_SESSION['id'], "*")){
                include 'admin/page/rank/rankButton.php';
            }
        }else{
            echo "<td></td>";
        } 
    ?>
</tr>