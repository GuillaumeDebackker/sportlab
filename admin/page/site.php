<?php
    require_once 'config/yml.class.php';
    require 'controller/data/config.php';
    $title = "Site";

    if(isset($_GET['save'])){
        $configLecture = new Lire('config/config.yml');
        $data = $configLecture->GetTableau();
        foreach($config as $Category){
            foreach($Category["content"] as $Content){
                if(!empty($_POST[$Content['id']])){
                    $data[$Content['config']] = $_POST[$Content['id']];
                }
            }
        }

        $success = "Les changement ont bien été pris en compte";
        $ecriture = new Ecrire('config/config.yml', $data);
    }
?>

<div class="content">
    <div class="container-fluid">

        <?php if(isset($success)){ ?>
            <div class="alert alert-success alert-dismissible">
                  <h5><i class="icon fa fa-check"></i> Reussite!</h5>
                  <?php echo $success; ?>
            </div>
        <?php } ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Configuration du site</h3>
            </div>
            <form action="?&admin=site&save=true" method="post">
                <div class="card-body">
                    <?php
                        $first = true;
                        foreach($config as $Category){
                    ?>
                        <?php 
                            if(!$first){
                                echo '<hr>';            
                            }
                            $first = false;
                        ?>
                        <div>
                            <h4 style="text-align:center;"><?php echo $Category["name"] ?></h4>

                            <div class="row" style="padding-top: 1em;">
                                    <?php 
                                        foreach($Category["content"] as $Content){
                                    ?>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="<?php echo $Content["id"] ?>"><?php echo $Content["name"] ?></label>
                                            <input type="text" class="form-control" name="<?php echo $Content["id"] ?>" id="<?php echo $Content["id"] ?>" placeholder="<?php echo $Content["name"] ?>">
                                        </div>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="card-footer">
                    <div style="padding-left:92%"> 
                        <button type="submit" class="btn btn-primary right">Sauvegarder</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>