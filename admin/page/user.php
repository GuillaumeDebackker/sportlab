<?php
    $title = "Joueur";

    if(isset($_GET['mode']) && $_GET['mode'] === "edit"){
        if($User->hasPermission($_SESSION['id'], "MODIFY_USERS")){
            header("location: index.php?&admin=modifyuser&id=".$_GET['id']);
        }else{
            $error = "Vous n'avez pas la permission de modifier un utilisateur";
        }
    }
?>

<div class="content">
    <div class="container-fluid">
        <div class="callout callout-warning">
            <h5>Attention !</h5>
            <p>Modifier un utilisateur n'est pas sans risque. L'utilisateur se connecte avec son adresse mail et d'un mot de passe. Le mieux est de le modifier avec son accord</p>
        </div>

        <?php if(isset($error)){ ?>
        <div class="alert alert-danger alert-dismissible">
            <h5><i class="icon fa fa-check"></i> Alert!</h5>
            <?php echo $error; ?>
        </div>
        <?php } ?>
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Liste d'utilisateur</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped" id="userList">
                    <thead>
                        <tr>
                            <th style="width:10px;">ID</th>
                            <th>Nom de famille</th>
                            <th>Prénom</th>
                            <th>email</th>
                            <th>Grade</th>
                            <th>Crée le</th>
                            <th style="width:100px;"></th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            foreach($User->getAllUsers() as $user){
                                include 'admin/page/user/userList.php';
                            } 
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>