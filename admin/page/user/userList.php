<tr>
    <td><?php echo $user['id'] ?></td>
    <td><?php echo $user['lastname'] ?></td>
    <td><?php echo $user['firstname'] ?></td>
    <td><?php echo $user['email'] ?></td>
    <td><?php echo $Rank->getRank($user['rank'])['name'] ?></td>
    <td><?php echo $user['createDate'] ?></td>
    <td>
        <a class="btn btn-info btn-sm" href="?&admin=user&mode=edit&id=<?php echo $user['id'] ?>">
            <i class="fa fa-pencil">
            </i>
            Editer
        </a>
    </td>
</tr>