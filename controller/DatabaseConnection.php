<?php


class DatabaseConnection
{

    private $bdd;
    public function __construct($Config)
    {
        try
        {
            $bdd = new PDO('mysql:host=' .$Config['url'] .';dbname=' .$Config['DatabaseName'].';port=' .$Config['DatabasePort'], $Config['DatabaseUser'], $Config['DatabasePassword']);

            // Cette requette SQL permet d'encoder correctement tout ce qui rentre / sort de la base.
            $bdd->exec("SET CHARACTER SET utf8");
            $this->bdd = $bdd;
        }
        catch (Exception $e)
        {
            echo $e;
        }
    }

    public function getConnection()
    {
        return $this->bdd;
    }
}