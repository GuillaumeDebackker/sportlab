<?php

class StringUtils{

    /**
     * Convert Array to String
     */
    public function convertArrayToString($Arrays, $key = false) : String{
        $string = "";
        foreach($Arrays as $element => $value){
            $string .= ($key ? $element : $value) . " ";
        }
        return $string;
    }

    public function convertStringToArray($String) : Array{
        return explode(" ", $String);
    }

    public function removeStringToString(string $string, $index, string $delimiter = " "): string
    {
        $stringParts = explode($delimiter, $string);
        if (is_array($index)) {
            foreach ($index as $i) {
                unset($stringParts[(int)($i)]);
            }
        } else {
            unset($stringParts[(int)($index)]);
        }
        return implode($delimiter, $stringParts);
    }

    public function removeElementToArray($Arrays,$remove) : Array{
        foreach($remove as $element){
            unset($Arrays[$element]);
        }
        return $Arrays;
    }

    public function hasString($haystack, $needle){
        foreach(explode(" ", $haystack) as $string){
            if($string === $needle){
                return true;
            }
        }
        return false;
    }
}