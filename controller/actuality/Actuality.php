<?php

class Actuality{

    private PDO $databaseConnection;

    public function __construct($databaseConnection){
        $this->databaseConnection = $databaseConnection;
    }

    public function getAllActuality(){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_actuality`");
        $prepare->execute();
        return $prepare->fetchAll();
    }

    public function getActuality($id){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_actuality` WHERE id = :id");
        $prepare->execute(Array("id" => $id));
        return $prepare->fetch();
    }

    public function addActuality($title,$textContent,$filename,$author){
        $prepare = $this->databaseConnection->prepare("INSERT INTO `sl_actuality`(title,textcontent,author,filename,createDate) VALUES (:title,:textcontent,:author,:filename,:date)");
        $prepare->execute(Array(
                "title" => $title,
                "textcontent" => $textContent,
                "author" => $author,
                "filename" => $filename,
                "date" => date("Y-m-d H:i:s")
        ));
    }

    public function removeActuality($id){
        $prepare = $this->databaseConnection->prepare("DELETE FROM `sl_actuality` WHERE id = :id");
        $prepare->execute(Array(
            "id" => $id
        ));
    }

    public function updateActuality($id, $key,$value){
        $sql = "UPDATE `sl_actuality` SET {$key} = '{$value}' WHERE id = {$id}";
        $prepare = $this->databaseConnection->prepare($sql);
        $prepare->execute();
    }

    public function addLikes($actuality,$user){
        $prepare = $this->databaseConnection->prepare("INSERT INTO `sl_likes`(actuality,user) VALUES (:actuality,:user)");
        $prepare->execute(Array(
                "actuality" => $actuality,
                "user" => $user,
        ));
    }

    public function removeLikes($actuality,$user){
        $prepare = $this->databaseConnection->prepare("DELETE FROM `sl_likes` WHERE actuality = :id AND user = :user");
        $prepare->execute(Array("id" => $actuality, "user" => $user));
    }

    public function getLike($actuality,$user){
        $sql = "SELECT * FROM `sl_likes` WHERE actuality = {$actuality} AND user = {$user};";
        $prepare = $this->databaseConnection->prepare($sql);
        $prepare->execute();
        return $prepare;
    }
}