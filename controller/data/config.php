<?php
$config = [
    0 => [
        "name" => "Configuration du site",
        "content" => [
            0 => [
                "id" => "namesite",
                "name" => "Nom du site",
                "config" => "nom"
            ],
            1 => [
                "id" => "description",
                "name" => "Description du site",
                "config" => "description"
            ],
            2 => [
                "id" => "template",
                "name" => "Template du site",
                "config" => "theme"
            ]
        ]
    ],
    1 => [
        "name" => "Réseaux sociaux",
        "content" => [
            0 => [
                "id" => "facebook",
                "name" => "Facebook",
                "config" => "facebook",
                "class" => "fa fa-facebook"
            ],
            1 => [
                "id" => "twitter",
                "name" => "Twitter",
                "config" => "twitter",
                "class" => "fa fa-twitter"
            ],
            2 => [
                "id" => "instagram",
                "name" => "Instagram",
                "config" => "instagram",
                "class" => "fa fa-instagram"
            ],
            3 => [
                "id" => "snapchat",
                "name" => "Snapchat",
                "config" => "snapchat",
                "class" => "fa fa-snapchat-ghost"
            ],
            4 => [
                "id" => "linkedin",
                "name" => "LinkedIn",
                "config" => "linkedin",
                "class" => "fa fa-linkedin"
            ]
        ]
    ]
];