<?php

    $permissions = [
        "*" => [
            "permission" => "*",
            "name" => "Toutes les permissions",
            "description" => "Vous avez accès à tous sur le site"
        ],
        "ACCESS_ADMIN" => [
            "permission" => "ACCESS_ADMIN",
            "name" => "Accès Administrateur",
            "description" => "Pour avoir accès au panel administrateur"
        ],
        "ACCESS_RANK" => [
            "permission" => "ACCESS_RANK",
            "name" => "Accès des grades",
            "description" => "Pour avoir accès a la liste des grades"
        ],
        "MODIFY_RANK" => [
            "permission" => "MODIFY_RANK",
            "name" => "modifier les grades",
            "description" => "Pour pouvoir modifier les informations d'un grade"
        ],
        "ACCESS_USERS" => [
            "permission" => "ACCESS_USERS",
            "name" => "Accès des utilisateurs",
            "description" => "Pour avoir accès a la liste des utilisateurs"
        ],
        "MODIFY_USERS" => [
            "permission" => "MODIFY_USERS",
            "name" => "modifier les utilisateurs",
            "description" => "Pour pouvoir modifier les informations d'un utilisateur"
        ],
        "ACCESS_WEB" => [
            "permission" => "ACCESS_WEB",
            "name" => "Accès au site",
            "description" => "Pour avoir accès au site"
        ]
    ];