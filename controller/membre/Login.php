<?php


class Login
{

    private PDO $databaseConnection;
    private $email, $password;

    public function __construct($databaseConnection, $email, $password)
    {
        $this->databaseConnection = $databaseConnection;
        $this->email = $email;
        $this->password = $password;
    }

    public function getUser()
    {
        $prepare = $this->databaseConnection->prepare("SELECT * FROM sl_users WHERE email = ?");
        $prepare->execute(array($this->email));
        if ($prepare->rowCount() > 0) {
            $user = $prepare->fetchAll()[0];
            if (password_verify($this->password, $user["password"])) {
                return $user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}