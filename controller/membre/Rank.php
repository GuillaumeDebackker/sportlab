<?php

class Rank
{

    private PDO $databaseConnection;

    public function __construct($databaseConnection){
        $this->databaseConnection = $databaseConnection;
    }

    public function getAllRanks(){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_rank`");
        $prepare->execute();
        return $prepare->fetchAll();
    }

    public function getRank($id){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_rank` WHERE id = :id");
        $prepare->execute(Array("id" => $id));
        return $prepare->fetch();
    }

     /**
     * update caroussel
     */
    public function updateRank($id,$name,$description,$permission,$color){
        $prepare = $this->databaseConnection->prepare("UPDATE `sl_rank` SET name=:name,description=:description,permission=:permission,color=:color WHERE id=:id");
        $prepare->execute(Array(
                "name" => $name,
                "description" => $description,
                "permission" => $permission,
                "color" => $color,
                "id" => $id
        ));
    }

    /**
     * add caroussel
     */
    public function addRank($name,$description,$permission,$color){
        $prepare = $this->databaseConnection->prepare("INSERT INTO `sl_rank`(name,description,permission,color) VALUES (:name,:description,:permission,:color)");
        $prepare->execute(Array(
                "name" => $name,
                "description" => $description,
                "permission" => $permission,
                "color" => $color
        ));
    }

    public function removeRank($id){
        $prepare = $this->databaseConnection->prepare("DELETE FROM `sl_rank` WHERE id = :id");
        $prepare->execute(Array(
            "id" => $id
        ));
    }
}