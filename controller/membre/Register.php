<?php

class Register
{

    private PDO $databaseConnection;
    private $lastname, $firstname, $email, $password,$birthday;

    public function __construct($databaseConnection,$lastname,$firstname, $email, $password,$birthday)
    {
        $this->databaseConnection = $databaseConnection;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->birthday = $birthday;
    }


    /**
     * CREATE USER AND CHECK IF UNIQUE EMAIL
     * return Array
     * Array("error" => ""; "success" => "");
     */
    public function createUser()
    {
        $array = [
            "success" => "",
            "error" => ""
        ];

        $prepare = $this->databaseConnection->prepare("SELECT * FROM sl_users WHERE email = ?");
        $prepare->execute(array($this->email));

        if($prepare->rowCount() > 0){
            $array['error'] = "Le mail est déjà utilisé";
        }else{
            try{
                $prepare = $this->databaseConnection->prepare("INSERT INTO sl_users(email,lastname,firstname,password,birthday,createDate) VALUES (:email,:lastname,:firstname,:password,:birthday,:date)");
                $prepare->execute(Array(
                    'email' => $this->email,
                    'lastname' => $this->lastname,
                    'firstname' => $this->firstname,
                    'password' => password_hash($this->password, PASSWORD_DEFAULT),
                    'birthday' => $this->birthday,
                    'date' => date("Y-m-d H:i:s")
                ));
            }catch(PDOException $e){
                echo $e;
            }
            $array['success'] = "Le compte a bien été crée";
        }
        return $array;
    }
}