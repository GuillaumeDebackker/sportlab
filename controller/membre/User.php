<?php

class User
{

    private PDO $databaseConnection;
    private Rank $Ranks;

    public function __construct($databaseConnection,$ranks){
        $this->databaseConnection = $databaseConnection;
        $this->Ranks = $ranks;
    }

    public function getAllUsers(){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_users`");
        $prepare->execute();
        return $prepare->fetchAll();
    }

    public function getUser($id){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_users` WHERE id = :id");
        $prepare->execute(Array("id" => $id));
        return $prepare->fetch();
    }

    public function updateUser($id, $key,$value){
        $sql = "UPDATE `sl_users` SET {$key} = {$value} WHERE id = {$id}";
        $prepare = $this->databaseConnection->prepare($sql);
        $prepare->execute();
    }

    public function getProfil($id,$Config){
        $user = $this->getUser($id);
        if(is_null($user[6])){
            return "theme/" . $Config['theme']. "/img/profil.png";
        }
        return "upload/" . $user['profil'];
    }

    public function hasPermission($id,$permission){
        $user =  $this->getUser($id);
        $rank = $this->Ranks->getRank($user['rank']);
        return ($rank['permission'] === "*" || StringUtils::hasString($rank["permission"], $permission));
    }
}