<?php

class Caroussel{

    private PDO $databaseConnection;

    public function __construct($databaseConnection){
        $this->databaseConnection = $databaseConnection;
    }

    /**
     * Get All Caroussel
     */
    public function getAllCaroussel(){
        $prepare = $this->databaseConnection->prepare("SELECT * FROM `sl_caroussel`");
        $prepare->execute();
        return $prepare->fetchAll();
    }

    /**
     * add caroussel
     */
    public function addCaroussel($title,$description,$filename){
        $prepare = $this->databaseConnection->prepare("INSERT INTO `sl_caroussel`(titre,description,filename) VALUES (:title,:description,:filename)");
        $prepare->execute(Array(
                "title" => $title,
                "description" => $description,
                "filename" => $filename
        ));
    }

    public function removeCaroussel($id){
        $prepare = $this->databaseConnection->prepare("DELETE FROM `sl_caroussel` WHERE id = :id");
        $prepare->execute(Array(
            "id" => $id
        ));
    }
}