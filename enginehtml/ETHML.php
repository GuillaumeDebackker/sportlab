<?php

include 'exception/EHTMLFileException.php';
include 'FileReader.php';
include 'balise/ManagerBalise.php';

class ETHML
{

    private $filename;
    private $title;
    private ManagerBalise $ManagerBalise;

    public function __construct($filename,$title = null)
    {
        $this->filename = $filename;
        $this->title = $title;
        $this->ManagerBalise = new ManagerBalise();
    }

    /**
     * @param $data
     * RENDER FORMAT HTML
     * @throws Exception
     */
    public function render($data = []) : void {
        // CHECK IF FILE IS EXIST AND IF FILE EHTML
        if(!file_exists($this->filename)) throw new EHTMLFileException($this->filename . " is not exists");
        if(pathinfo($this->filename, PATHINFO_EXTENSION) != "ehtml") throw new EHTMLFileException($this->filename . " is not Engine HTML extensions");

        $fileReader = new FileReader($this->filename);
        $contentArray = $fileReader->getContentArray();

        if ($this->title != null) $data["title"] = $this->title;

        echo $this->ManagerBalise->convertBalise($contentArray,$data);
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }
}