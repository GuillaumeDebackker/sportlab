<?php


class FileReader
{

    private $filename;
    private $file;

    public function __construct($filename)
    {
        $this->filename = $filename;
        $this->file = fopen($filename, "r");
    }

    /**
     * @return content File
     */
    public function getContents() : string {
        return file_get_contents($this->filename);
    }

    public function getContentArray() : array
    {
        $array = [];
        while(!feof($this->file)) {
            array_push($array, fgetc($this->file));
        }
        return $array;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename): void
    {
        $this->filename = $filename;
    }
}