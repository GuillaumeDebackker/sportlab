<?php


interface IBalise
{
    public function getVariables() : array;
    public function getResult() : string;
}