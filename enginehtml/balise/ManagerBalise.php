<?php
include "BaliseEHTML.php";
class ManagerBalise
{
    private $array;
    private $BaliseArray;

    public function __construct()
    {
        $this->array = [];
        $this->BaliseArray = [];
        $this->generateBalise();
    }


    public function convertBalise(array $contentArray,array $data = []) : string {
        //$this->console_log($contentArray,true);

        $this->BaliseArray = $this->getBalise($contentArray);

        $this->console_log($this->BaliseArray,true);
        return $this->getContent($contentArray);
    }

    public function getBalise($contentArray): array
    {
        $ArrayBalise = [];
        $baliseCalled = false;
        $balise = "";
        $hasComment = false;
        for ($x = 0; $x < count($contentArray);$x++){
            if($contentArray[$x] == "<") $baliseCalled = true;

            if ($contentArray[$x] == "!" && $contentArray[$x - 1] == "<"){
                $hasComment = true;
                $baliseCalled = false;
                $balise = "";
            }

            if($baliseCalled == true){
                $balise = $balise . $contentArray[$x];
            }

            if($contentArray[$x] == ">") {
                if($hasComment){
                    $hasComment = false;
                    continue;
                }
                array_push($ArrayBalise, $balise);
                $balise = "";
                $baliseCalled = false;
            }
        }
        return $ArrayBalise;
    }

    function console_log($data, $add_script_tags = false) {
        $command = 'console.log('. json_encode($data, JSON_HEX_TAG).');';
        if ($add_script_tags) {
            $command = '<script>'. $command . '</script>';
        }
        echo $command;
    }

    public function getContent($contentArray) : string{
        $content = "";
        for ($x = 0; $x < count($contentArray);$x++){
            $content = $content . $contentArray[$x];
        }
        return $content;
    }

    private function generateBalise(): void
    {
        //$this->createBalise(new BaliseEHTML("test"));
    }

    private function createBalise($class) : void {
        $this->array[strval($class->getName())] = $class;
    }
}