<?php
    ob_start();
    session_start();
    //session_unset();
    require_once 'controller/config.php';

    // CMS NOT INSTALLED
    if(!$Config["installation"]){
        header("location: install/");
    }

    require_once 'controller/DatabaseConnection.php';
    require_once 'controller/membre/Rank.php';
    require_once 'controller/membre/User.php';
    require_once 'controller/actuality/Actuality.php';
    require_once "controller/StringUtils.php";

    $databaseConnection = new DatabaseConnection($Config['database']);
    $databaseConnection = $databaseConnection->getConnection();

    $Rank = new Rank($databaseConnection);
    $User = new User($databaseConnection,$Rank);
    $Actuality = new Actuality($databaseConnection);

    if(isset($_GET['admin'])){
        // CHECK IF ACCESS PANEL ADMIN
        if(empty($_SESSION['id']) || !$User->hasPermission($_SESSION['id'],"ACCESS_ADMIN")){
            header('location: index.php');
            return;
        }

        $adminPage = 'admin/page/' . $_GET['admin'] . '.php';
        if(file_exists($adminPage)){
            require $adminPage;
            $contentAdmin = ob_get_clean();

            include 'admin/index.php';
        }else{
            include 'theme/'. $Config['theme'].'/404.php';
        }
    }else if(isset($_GET['post'])){
        include 'theme/'. $Config['theme'].'/page/post/'.$_GET['post']. ".php";
    }else{
        if(!isset($_GET['page'])) $_GET['page'] = 'home';

        $filePage = 'theme/'. $Config['theme']. '/page/' . ($_GET['page'] === "home" ? "index" : $_GET['page']) . '.php';
        if(file_exists($filePage)){
            require $filePage;
            $content = ob_get_clean();

            include 'theme/'. $Config['theme'].'/index.php';
        }else{
            include 'theme/'. $Config['theme'].'/404.php';
        }
    }

    ob_end_flush();
?>
