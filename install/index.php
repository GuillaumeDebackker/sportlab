<?php
require_once '../config/yml.class.php';

$configLecture = new Lire('../config/config.yml');
$Config = $configLecture->GetTableau();

$install = new Lire('install.yml');
$install = $install->GetTableau();
$installEtape = $install['etape'];

if(isset($_GET['action']) and $_GET['action'] == $install['nameEtape'][1]){
    if(!empty($_POST['host']) and !empty($_POST['user']) and !empty($_POST['nomBase']) and !empty($_POST['port'])){
        try{
            $sql = new PDO('mysql:host='.$_POST['host'].';dbname='.$_POST['nomBase'].';port='.$_POST['port'], $_POST['user'], (empty($_POST['password']) ? '' : $_POST['password']));
            $sql->exec("SET CHARACTER SET utf8");
            require_once 'sql/InstallSQL.php';
        }catch (PDOException $e){
            $dataError = "Vous avez une erreur avec votre base de donnée";
        }
    }else{
        $dataError = "Vous devez remplir tous les champs";
    }
}

if (isset($_GET['action']) and $_GET['action'] == $install['nameEtape'][2]){
    if(!empty($_POST['name']) and !empty($_POST['description'])){
        require_once 'information/InstallInfos.php';
    }else{
        $dataError = "Vous devez remplir tous les champs";
    }
}

if (isset($_GET['action']) and $_GET['action'] == $install['nameEtape'][3]){
    if(!empty($_POST['lastname']) and !empty($_POST['firstname']) and !empty($_POST['email']) and !empty($_POST['password'])){
        $sql = new PDO('mysql:host='.$Config['database']['url'].';dbname='.$Config['database']['DatabaseName'].';port='.$Config['database']['DatabasePort'], $Config['database']['DatabaseUser'], (empty($Config['database']['DatabasePassword']) ? '' : $Config['database']['DatabasePassword']));
        $sql->exec("SET CHARACTER SET utf8");
        require_once 'information/InstallAdmin.php';
    }else{
        $dataError = "Vous devez remplir tous les champs";
    }
}

if(isset($_GET['action']) and $_GET['action'] == $install['nameEtape'][4]){
    $configLecture = new Lire('../config/config.yml');
    $config = $configLecture->GetTableau();

    $config['installation'] = true;
    $ecriture = new Ecrire('../config/config.yml',$config);

    header('location: ../index.php');
}
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../theme/<?php echo $Config['theme']?>/css/bootstrap.css">
    <link rel="stylesheet" href="../theme/<?php echo $Config['theme']?>/css/install.css">
    <link rel="stylesheet" href="../theme/<?php echo $Config['theme']?>/css/animate.css">
    <title>SportLab | Installation #<?php echo $installEtape ?></title>
</head>
<body class="container">
    <div class="well well-install">
        <h1 class="animated slideInLeft" style="font-family: material;text-align: center;margin-bottom: 25px;">SportLab</h1>
        <div class="p-install text-center">
            <h1>Merci d'avoir choisi SportLab !</h1>
            <p>Des mises à jour seront disponibles très fréquemment sur le site officiel.<br/>
                Il peut néanmoins y avoir des bugs ! Merci de les report sur le forum pour les corriger au plus vite.<br/>
                Suivez les instructions pour installer SportLab.<br/>
            </p>
            <p><a href="<?php echo $Config['licence'] ?>" class="btn btn-primary btn-installation" role="button">Aller sur <?php echo $Config['licence'] ?></a></p>
        </div>
        <br>

        <div class="p-install-form">
            <div class="container" style="width: 90%; margin: 10px auto;">
                <div class="row multistep">
                    <div class="col-xs-3 multistep-step <?php if($installEtape == 1) { echo 'current'; } elseif($installEtape >= 1) { echo 'complete'; } ?>">
                        <div class="text-center multistep-stepname" style="color: white;">Étape #1<br/><small>Mise en place BDD SQL</small></div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="multistep-dot"></a>
                    </div>

                    <div class="col-xs-3 multistep-step <?php if($installEtape == 2) { echo 'current'; } elseif($installEtape >= 2) { echo 'complete'; } ?>">
                        <div class="text-center multistep-stepname" style="color: white;">Étape #2<br/><small>Configuration site</small></div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="multistep-dot"></a>
                    </div>

                    <div class="col-xs-3 multistep-step <?php if($installEtape == 3) { echo 'current'; } elseif($installEtape >= 3) { echo 'complete'; } ?>">
                        <div class="text-center multistep-stepname" style="color: white;">Étape #3<br/><small>Création compte admin</small></div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="multistep-dot"></a>
                    </div>

                    <div class="col-xs-3 multistep-step <?php if($installEtape == 4) { echo 'current'; } elseif($installEtape >= 4) { echo 'complete'; } ?>">
                        <div class="text-center multistep-stepname" style="color: white;">Étape #4<br/><small>Fini c:</small></div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="multistep-dot"></a>
                    </div>
                </div>
            </div>
        </div>

        <form method="post" action="?&action=<?php echo $install['nameEtape'][$installEtape]?>">
            <div class="row">
                <?php
                    if(isset($dataError)){
                        echo '<div class="alert alert-danger text-center" style="margin-top: 30px;">'.$dataError. '</div>';
                    }
                    if($installEtape == 1){
                ?>
                <h3 style="font-family: material;text-align: center;margin-top: 40px;">Base de données</h3>
                <div class="form-group col-md-6">
                    <label>Adresse de la base de donnée</label>
                    <input type="text" name="host" class="form-control form-install" placeholder="Exemple: sql.hebergeur.fr"/>
                </div>
                <div class="form-group col-md-6">
                    <label>Utilisateur</label>
                    <input type="text" name="user" class="form-control form-install" placeholder="Exemple: monutilisateur864"/>
                </div>
                <div class="form-group col-md-6">
                    <label>Mot de passe</label>
                    <input type="password" name="password" class="form-control form-install" placeholder="Exemple: ttcpgm18"/>
                </div>
                <div class="form-group col-md-6">
                    <label>Nom de la base</label>
                    <input type="text" name="nomBase" class="form-control form-install" placeholder="Exemple: cmw_bdd"/>
                </div>
                <div class="form-group col-md-6">
                    <label>Port de connection <small>Si inconnu laissez 3306</small></label>
                    <input type="text" name="port" class="form-control form-install" value="3306"/>
                </div>
                <div class="form-group col-md-6">
                    <input type="submit" class="btn btn-success btn-installation btn-valider"/>
                </div>
                <?php }elseif ($installEtape == 2){?>
                <h3 style="font-family: material;text-align: center;margin-top: 40px;">Information du site</h3>
                <div class="form-group col-md-6">
                    <label>Nom du Site/Serveur</label>
                    <input type="text" name="name" class="form-control form-install" placeholder="Exemple: Sportlab"/>
                </div>
                <div class="form-group col-md-6">
                    <label>Une petite description</label>
                    <input type="text" name="description" class="form-control form-install" placeholder="Exemple: Sportlab est un site CMS"/>
                </div>
                <div class="form-group col-md-4">
                    <input type="submit" class="btn btn-success btn-installation btn-valider"/>
                </div>
                <?php }elseif ($installEtape == 3){ ?>
                    <h3 style="font-family: material;text-align: center;margin-top: 40px;">Creation du compte admin</h3>
                    <div class="form-group col-md-6">
                        <label>Prénom</label>
                        <input type="text" name="firstname" class="form-control form-install" placeholder="Exemple: SportLab"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Nom</label>
                        <input type="text" name="lastname" class="form-control form-install" placeholder="Exemple: SportLab"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Adresse Email</label>
                        <input type="email" name="email" class="form-control form-install" placeholder="Exemple: contact@sportlab.fr">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Mot de passe</label>
                        <input type="password" name="password" class="form-control form-install" placeholder="Exemple: ttcpgm18000"/>
                    </div>
                    <div class="form-group col-md-6">
                        <input type="submit" class="btn btn-success btn-installation btn-valider"/>
                    </div>
                <?php } else{?>
                        <h3 style="font-family: material;text-align: center;margin-top: 40px;">Votre site est fini</h3>
                        <input type="submit" class="btn btn-success btn-installation btn-valider"/>

                <?php } ?>
            </div>
        </form>
        <div class="footer">
            Copyright © <a href="http://sportlab.fr">SportLab</a> 2020
        </div>
    </div>
    <script src="../theme/<?php echo $Config['theme']; ?>/js/jquery.js"></script>
    <script src="../theme/<?php echo $Config['theme']; ?>/js/bootstrap.min.js"></script>
</body>
</html>
