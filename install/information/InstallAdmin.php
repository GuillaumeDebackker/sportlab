<?php
$request = $sql->prepare("INSERT INTO `sl_users`(lastname,firstname,password,email,rank,createDate) VALUES (:lastname,:firstname,:password,:email,2,:date)");
$request->execute(Array (
    'lastname' => $_POST['lastname'],
    'firstname' => $_POST['firstname'],
    'password' => password_hash($_POST['password'],PASSWORD_DEFAULT),
    'email' => $_POST['email'],
    'date' => date("Y-m-d H:i:s")
));

$installLecture = new Lire('install.yml');
$installLecture = $installLecture->GetTableau();
$installLecture['etape'] = 4;

$ecriture = new Ecrire('install.yml', $installLecture);

header('location: index.php');