<?php

$configLecture = new Lire('../config/config.yml');
$config = $configLecture->GetTableau();

$config['nom'] = $_POST['name'];
$config['description'] = $_POST['description'];

$ecriture = new Ecrire('../config/config.yml', $config);

$installLecture = new Lire('install.yml');
$installLecture = $installLecture->GetTableau();
$installLecture['etape'] = 3;

$ecriture = new Ecrire('install.yml', $installLecture);

header('location: index.php');