<?php
$sql->query(file_get_contents('sql/install.sql'))->execute();

$configLecture = new Lire('../config/config.yml');
$config = $configLecture->GetTableau();

$config['database']['url'] = $_POST['host'];
$config['database']['DatabaseName'] = $_POST['nomBase'];
$config['database']['DatabaseUser'] = $_POST['user'];
$config['database']['DatabasePassword'] = $_POST['password'];
$config['database']['DatabasePort'] = $_POST['port'];

$ecriture = new Ecrire('../config/config.yml', $config);

$installLecture = new Lire('install.yml');
$installLecture = $installLecture->GetTableau();
$installLecture['etape'] = 2;

$ecriture = new Ecrire('install.yml', $installLecture);

header('location: index.php');