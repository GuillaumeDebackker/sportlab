-- TABLE START WITH -> sl

CREATE TABLE IF NOT EXISTS `sl_users`(
    id int NOT NULL AUTO_INCREMENT,
    email varchar(255) UNIQUE NOT NULL,
    lastname varchar(255) NOT NULL,
    firstname varchar(255) NOT NULL,
    password varchar(200) NOT NULL,
    rank int(10) NOT NULL DEFAULT '1',
    profil varchar(255),
    birthday DATE NOT NULL,
    createDate DATETIME NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `sl_rank`(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(20) NOT NULL,
    description text,
    permission LONGTEXT DEFAULT 'ACCESS_WEB',
    color ENUM('rouge','bleu','vert','noir','gris','jaune','orange') NOT NULL DEFAULT 'gris',
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `sl_caroussel`(
    id int NOT NULL AUTO_INCREMENT,
    titre varchar(255) NOT NULL,
    description text NOT NULL,
    filename varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `sl_actuality`(
    id int NOT NULL AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    textcontent LONGTEXT NOT NULL,
    author int NOT NULL,
    filename varchar(255),
    likes int DEFAULT 0,
    createDate DATETIME NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `sl_likes`(
    id int NOT NULL AUTO_INCREMENT,
    actuality int NOT NULL,
    user int NOT NULL,
    PRIMARY KEY (`id`)
);

INSERT INTO `sl_rank`(name,description,permission) VALUES ("membre","Membre du site","ACCESS_WEB");
INSERT INTO `sl_rank`(name,description, permission) VALUES ("SportLab","Membre de sportlab", "*");