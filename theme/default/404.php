<?php
?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $Config['nom'];?> | 404</title>


    <style>
        body{
            margin: 0px;
            padding: 0px;

            text-align: center;
            font-size: 5em;
            background-color: #262626;
        }
        .content {
            max-width: 50%;
            margin: auto;
            padding-top: 50px;
        }
    </style>
</head>
<body>
    <div class="content">
        <h1>404</h1>
        <h3>Page not found</h3>
        <a href="index.php">Retourner sur le site</a>
    </div>
</body>
</html>
