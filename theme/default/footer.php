<!-- Site footer -->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h6>A propos</h6>
                <p class="text-justify"><?php echo $Config['description'] ?></p>
            </div>
            <div class="col-xs-6 col-md-3"></div>
            <div class="col-xs-6 col-md-3">
                <h6>Liens rapides</h6>
                <ul class="footer-links">
                    <li><a href="">A propos</a></li>
                    <li><a href="">Nous contacter</a></li>
                </ul>
            </div>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; <?php echo date("Y"); ?> All Rights Reserved by
                    <a href="#">SPORTLAB</a>.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <?php
                        require 'controller/data/config.php';
                        foreach($config[1]["content"] as $network){
                            if(!empty($Config[$network['id']])){
                    ?>
                        <li><a class="<?php echo $network['id']; ?>" href="<?php echo $Config[$network['id']]; ?>"><i class="<?php echo $network['class']; ?>"></i></a></li>
                    <?php }} ?>
                </ul>
            </div>
        </div>
    </div>
</footer>