<nav class="navbar navbar-expand-lg navbar-dark navbar-custom">
    <a class="navbar-brand" href="#">SPORTLAB</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav navbar-center">
            
            <?php
                $link = array(
                        "Accueil" => "home",
                        "Actualité" => "actuality"
                );

                foreach ($link as $key => $value) {
                    echo '<li class="nav-item'. ($_GET['page'] === $value ? ' active' : '') . '">';
                    echo '<a class="nav-link'.($_GET['page'] === $value ? ' active' : '') .'" href="?&page='.$value.'">'.$key.'</a>';
                    echo '</li>';
                }

            ?>
        </ul>

        <?php
            if(isset($_SESSION['id'])){
        ?>
        <div class="navbar-right">
            <div class="dropdown">
                <button class="dropdown-toggle navbar-button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="theme/<?php echo $Config['theme'] ?>/img/profil.png">
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="?&page=profil">Profile</a>
                    <?php
                        require 'controller/data/permissions.php';

                        if($User->hasPermission($_SESSION['id'], $permissions['ACCESS_ADMIN']['permission'])){
                            echo '<a class="dropdown-item" href="?&admin=index">Admin</a>';
                        }
                    ?>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="?&page=disconnect">Deconnexion</a>
                </div>
            </div>
        </div>

        <?php }else{ ?>
            <div class="navbar-right">
                <a href="?&page=connexion" class="button button-login">S'enregistrer</a>
            </div>
        <?php } ?>
    </div>
</nav>