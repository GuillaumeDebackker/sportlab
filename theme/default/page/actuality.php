<?php
    $title = "Actualité";
?>

<div class="container" style="padding-top: 2em">
    <h3 class="text-center">Actualité</h3>

    <?php if(isset($_GET['view'])){
        $actuality = $Actuality->getActuality($_GET['view']);
        $user = $User->getUser($actuality['author']);
        include "theme/". $Config['theme']. "/page/actuality/actualityView.php";
    }else{
        echo '<div class="row">';
        foreach($Actuality->getAllActuality() as $actuality){
            include "theme/". $Config['theme']. "/page/actuality/actualityProfil.php";
        }
        echo '</div>';
    }
    ?>
</div>