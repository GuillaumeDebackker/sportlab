<i class="fa <?php if(empty($Actuality->getLike($actuality['id'], $_SESSION['id'])->fetchAll()[0])){ echo 'fa-heart-o';} else{echo 'fa-heart';} ?>" id="like" style="cursor: pointer;user-select: none;" onclick="updateLike()"></i>
<span id="numberLike"><?php echo $actuality['likes'] ?></span>

<script>
    function updateLike(){
        var like = document.getElementById("like");
        var numberLike = document.getElementById("numberLike");
        $.ajax({
            url: "?&post=actualitylike",
            type: 'POST',
            dataType: 'json',
            data: {
                actuality : <?php echo $actuality['id']; ?>
            },
            success: function(result){
                console.log(result);
                like.className = result["class"];
                numberLike.innerHTML = result["likes"];
            }    
        });
    }
</script>