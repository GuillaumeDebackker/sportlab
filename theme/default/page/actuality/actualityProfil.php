<div class="col-3" style="width: 18rem;">
    <div class="post-module">
        <div class="thumbnail">
            <div class="date">
            <div class="day"><?php echo date("d",strtotime($actuality['createDate'])); ?>/<?php echo date("m",strtotime($actuality['createDate'])); ?></div>
            </div><img src="upload/<?php echo $actuality['filename']; ?>"/>
        </div>
                    <!-- Post Content-->
        <div class="post-content">
            <div class="category">Nouvelles</div>
            <h1 class="title"><?php echo $actuality['title']; ?></h1>
            <h2 class="sub_title">Pour savoir l'information, vous devez cliquer sur voir</h2>
            <div class="post-meta"><a href="?&page=actuality&view=<?php echo $actuality['id']; ?>" class="btn btn-primary">Voir</a></div>
        </div>
    </div>
</div>