<div class="row" style="padding-top:2em;">
    <div class="col-3">
        <div class="card round">
            <div class="card-body">
                <h3 class="card-title text-center black">Auteur</h3>
                <p class="text-center"><img src="theme/<?php echo $Config['theme'] ?>/img/profil.png" class="circle" style="height:106px;width:106px" alt="Avatar"></p>
                <hr>
                <p><i class="fa fa-pencil"></i> <?php echo $user['firstname'] . " " . $user['lastname']; ?></p>
                <p><i class="fa fa-graduation-cap"></i> <span class="badge" style="background-color: <?php 
                    require 'controller/data/color.php';
                    echo $colors[$Rank->getRank($user['rank'])['color']];
                ?>"><?php echo $Rank->getRank($user['rank'])['name']; ?></span></p>
                <p><i class="fa fa-birthday-cake"></i> <?php echo date("d M y",strtotime($user['birthday'])); ?></p>
            </div>
        </div>
    </div>

    <div class="col-9">
        <div class="card round">
            <div class="card-body">
                <span style="float:right;"><?php echo date("d M 20y",strtotime($actuality['createDate'])); ?></span>
                <h4 class="black"><?php echo $actuality['title']; ?></h4>
                <hr>
                <p><?php echo $actuality['textcontent']; ?></p>
                <hr>
                <?php include 'theme/'.$Config['theme'].'/page/actuality/actualityButtonLike.php'; ?>
            </div>
        </div>
    </div>
</div>