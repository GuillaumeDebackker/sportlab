<?php
    require 'controller/membre/Login.php';
    require 'controller/membre/Register.php';

    $title = "Connexion";

    if(isset($_GET['mode'])){
        if($_GET['mode'] === "login"){
            $email = $_POST['inputEmailLogin'];
            $password = $_POST['inputPasswordLogin'];
            $remember = $_POST['remember'];
                
            $login = new Login($databaseConnection,$email,$password);
            $user = $login->getUser();

            if(is_null($user)){
                $errorLogin = "Mail/password est incorrect";
            }else{
                $_SESSION['id'] = $user['id'];
                $_SESSION['remember'] = $remember;
                header('location: ../index.php');
            }
        }

        else if($_GET['mode'] === "register"){
            $lastname = $_POST['lastname'];
            $firstname = $_POST['firstname'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $passwordConfirm = $_POST['passwordConfirm'];
            $birthday = $_POST['birthday'];

            if(strcmp($password,$passwordConfirm) !== 0){
                $errorRegister = "Les mots de passe ne correspond pas";
            }else{
                $register = new Register($databaseConnection,$lastname,$firstname,$email,$password,$birthday);
                $data = $register->createUser();

                if(!empty($data['success'])) $success = $data['success'];
                if(!empty($data['error'])) $errorRegister = $data['error'];
            }
        }
    }
?>


<div class="container" style="padding-top: 2em">

    <h1 class="text-center white">Connexion/S'enregistrer</h1>

    <?php
        if(isset($success)){
    ?>

    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong>Réussite! </strong> <?php echo $success ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php
        }
    ?>

    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Connexion</h5>

                    <?php
                        if(isset($errorLogin)){
                    ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>ERREUR! </strong> <?php echo $errorLogin ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php
                        }
                    ?>

                    <form class="form-signin" method="post" action="?&page=connexion&mode=login">
                        <div class="form-label-group">
                            <input type="email" id="inputEmailLogin" name="inputEmailLogin" class="form-control" placeholder="Email" required autofocus>
                            <label for="inputEmailLogin">Email</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputPasswordLogin" name="inputPasswordLogin" class="form-control" placeholder="Mot de passe" required>
                            <label for="inputPasswordLogin">Mot de passe</label>
                        </div>

                        <a href="" class="password-forgot">Mot de passe oublié ?</a>

                        <div class="custom-control custom-checkbox mb-3" style="padding-top: 1em;">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember">
                            <label class="custom-control-label" for="customCheck1">Se souvenir de moi</label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Connexion</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">S'enregistrer</h5>

                    <?php
                    if(isset($errorRegister)){
                        ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>ERREUR! </strong> <?php echo $errorRegister ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php
                            }
                        ?>

                    <form class="form-signin" method="post" action="?&page=connexion&mode=register">
                        <div class="form-label-group">
                            <input type="text" id="inputLastName" name="lastname" class="form-control" placeholder="Nom de famille" required autofocus>
                            <label for="inputLastName">Nom de famille</label>
                        </div>
                        <div class="form-label-group">
                            <input type="text" id="inputFirstName" name="firstname" class="form-control" placeholder="Prénom" required autofocus>
                            <label for="inputFirstName">Prénom</label>
                        </div>
                        <div class="form-label-group">
                            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email" required autofocus>
                            <label for="inputEmail">Email</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="mot de passe" required>
                            <label for="inputPassword">mot de passe</label>
                        </div>

                        <div class="form-label-group">
                            <input type="password" id="inputPasswordConfirm" name="passwordConfirm" class="form-control" placeholder="Comfirmer mot de passe" required>
                            <label for="inputPasswordConfirm">Comfirmer mot de passe</label>
                        </div>
                        
                        <div class="form-label-group">
                            <input type="date" id="birthday" name="birthday" class="form-control" placeholder="Anniversaire" required>
                            <label for="inputPasswordConfirm">Anniversaire</label>
                        </div>

                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>