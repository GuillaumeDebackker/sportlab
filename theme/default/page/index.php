<?php
    require 'controller/view/Caroussel.php';
    $title = 'Accueil';
    $Caroussel = new Caroussel($databaseConnection);
?>

<div class="content">
  <?php if(count($Caroussel->getAllCaroussel(), COUNT_RECURSIVE) > 0) { ?>
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <?php
        $active = true;
        foreach($Caroussel->getAllCaroussel() as $caroussel){
      ?>
        <li data-target="#carouselExampleCaptions" data-slide-to="<?php echo $caroussel['id'] ?>" <?php if($active){ echo 'class="active"'; } ?>></li>
        <?php $active = false ?>
      <?php } ?>
    </ol>
    <div class="carousel-inner">
      <?php
          $active = true;
          foreach($Caroussel->getAllCaroussel() as $caroussel){
      ?>
        <div class="carousel-item <?php if($active){ echo 'active'; }?>">
          <img src="upload/<?php echo $caroussel['filename'] ?>" class="d-block w-100" alt="..." style="height: 700px;">
          <div class="carousel-caption d-none d-md-block">
            <h5><?php echo $caroussel['titre'] ?></h5>
            <p><?php echo $caroussel['description'] ?></p>
          </div>
        </div>
        <?php $active = false ?>
      <?php } ?>
    </div>
  </div>

  <?php }else{ ?>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner home">
        <div class="carousel-item active">
            <img src="theme/<?php echo $Config['theme'] ?>/img/background_install.jpg" alt="">
        </div>
      </div>
    </div>
  <?php } ?>
</div>