<?php
    $array = [];
    if($_SESSION['id']){
        $actuality = $Actuality->getActuality($_POST['actuality']);
        if(empty($Actuality->getLike($actuality['id'], $_SESSION['id'])->fetchAll()[0])){
            $array['class'] = "fa fa-heart";
            $actuality['likes'] += 1;
            $Actuality->updateActuality($actuality['id'], "likes", $actuality['likes']);
            $Actuality->addLikes($actuality['id'],$_SESSION['id']);
        }else{
            $array['class'] = "fa fa-heart-o";
            $actuality['likes'] -= 1;
            $Actuality->updateActuality($actuality['id'], "likes", $actuality['likes']);
            $Actuality->removeLikes($actuality['id'],$_SESSION['id']);
        }
        $array['actuality'] = $Actuality->getLike($actuality['id'], $_SESSION['id']);
        $array['data'] = $Actuality->getLike($actuality['id'], $_SESSION['id'])->fetchAll();
        $array['likes'] = $actuality['likes'];
        echo json_encode($array);
    }
?>