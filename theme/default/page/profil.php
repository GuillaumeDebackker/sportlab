<?php
    $title = "Profile";
    $user = $User->getUser($_SESSION['id']);
?>

<div class="container" style="padding-top: 2em">
    <h1 class="text-center white">Profile</h1>

    <div class="row">
        <div class="col-2"></div>
        <div class="col-8">
            <div class="card">
                <form method="POST" action="" class="card-body">
                    <h4 class="black">Paramètre du compte</h4>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-label-group">
                                <input type="text" id="firstname" name="firstname" class="form-control" value="<?php echo $user['firstname']; ?>" required autofocus>
                                <label for="firstname">Prénom</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-label-group">
                                <input type="text" id="lastname" name="lastname" class="form-control" value="<?php echo $user['lastname']; ?>" required autofocus>
                                <label for="lastname">Nom de famille</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-label-group">
                                <input type="password" id="password" name="password" class="form-control" autofocus>
                                <label for="password">Mot de passe</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-label-group">
                                <input type="date" id="birthday" name="birthday" class="form-control" value="<?php echo $user['birthday']; ?>" required>
                                <label for="inputPasswordConfirm">Anniversaire</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputFile">Mettre un fichier</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile">Choisir un fichier</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                </form>
            </div>

        </div>
    </div>
</div>